/**
 * Created by macdja38 on 2016-06-13.
 */
"use strict";

var Utils = require('../lib/utils');
var utils = new Utils();

module.exports = class rank {
    constructor(e) {
        this.client = e.client;
        this.config = e.configDB;
        this.raven = e.raven;

        this.onJoin = (server, user) => {
            var rank = this.config.get("roles", false, {server: server.id});
            if (rank && rank.joinrole) {
                rank = server.roles.get("id", rank.joinrole);
                if (rank) {
                    this.client.addMemberToRole(user, rank, (error)=> {
                        if (error) {
                            let logChannel = this.config.get("msgLog", false, {server: server.id});
                            if (logChannel) {
                                logChannel = server.channels.get("id", logChannel);
                                if (logChannel) {
                                    this.client.sendMessage(logChannel, `Erro ${error} ao promover ${user}. Por favor, redefina o seu rank e veja se o bot tem permissões suficientes.`)
                                }
                            }
                        } else {
                            let logChannel = this.config.get("msgLog", false, {server: server.id});
                            if (logChannel) {
                                logChannel = server.channels.get("id", logChannel);
                                if (logChannel) {
                                    this.client.sendMessage(logChannel, `${utils.clean(user.username)} foi promovido a ${utils.clean(rank.name)}!`)
                                }
                            }
                        }
                    })
                }
            }
        };
    }

    onDisconnect() {
        this.client.removeListener("serverNewMember", this.onJoin);
    }

    onReady() {
        this.client.on("serverNewMember", this.onJoin);
    }

    getCommands() {
        return ["rank"];
    }

    onCommand(msg, command, perms) {
        console.log("autoRank");
        if (command.command === "rank") {
            if (command.args[0] === "add" && perms.check(msg, "admin.rank.add")) {
                let roleId;
                if (command.options.group && !command.options.role) {
                    command.options.role = command.options.group;
                }
                if (command.options.role) {
                    console.log(command.options.role);
                    if (/<@&\d+>/.test(command.options.role)) {
                        console.log("Found role mention");
                        roleId = msg.channel.server.roles.get("id", command.options.role.match(/<@&(\d+)>/)[1]);
                    }
                    else {
                        roleId = msg.channel.server.roles.get("name", command.options.role);
                    }
                    if (roleId) {
                        roleId = roleId.id
                    }
                    else {
                        msg.reply("Não foi possível encontrar um cargo com este nome. Use @menção ou o nome completo. Nomes diferenciam maiúsculas e minúsculas.");
                        return true;
                    }
                    let roleName = command.args[1].toLowerCase();
                    let oldRoles = this.config.get("roles", {}, {server: msg.server.id});
                    oldRoles[roleName] = roleId;
                    this.config.set("roles", oldRoles, {server: msg.server.id});
                    msg.reply(":thumbsup::skin-tone-2:");
                    return true;
                }
                return true;
            }
            if (command.args[0] === "remove" && perms.check(msg, "admin.rank.remove")) {
                if (!command.args[1]) {
                    msg.reply(`Por favor, especifique um rank a remover com \`${command.prefix}rank remove \<rank\>\`, para uma lista dos ranks use \`${command.prefix}rank list\``);
                    return true;
                }
                let rankToJoin = command.args[1].toLowerCase();
                let oldRoles = this.config.get("roles", {}, {server: msg.server.id});
                if (oldRoles.hasOwnProperty(rankToJoin)) {
                    delete oldRoles[rankToJoin];
                    this.config.set("roles", oldRoles, {server: msg.server.id});
                    msg.reply(":thumbsup::skin-tone-2:");
                } else {
                    msg.reply(`Cargo não pôde ser encontrado, use \`${command.prefix}rank list\` para ver os ranks atuais.`);
                }
                return true;
            }
            if (command.args[0] === "list" && perms.check(msg, "rank.list")) {
                let roles = this.config.get("roles", {}, {server: msg.server.id});
                let coloredRolesList = "";
                for (var role in roles) {
                    if (roles.hasOwnProperty(role) && role != "joinrole") {
                        if (perms.check(msg, `rank.join.${role}`)) {
                            coloredRolesList += `+${role}\n`;
                        } else {
                            coloredRolesList += `-${role}\n`;
                        }
                    }
                }
                if (coloredRolesList != "") {
                    msg.channel.sendMessage(`Os cargos que você pode entrar estão destacados em verde \`\`\`diff\n${coloredRolesList}\`\`\``)
                } else {
                    msg.reply(`Nenhum rank que você possa entrar.`)
                }
                return true;
            }
            if (command.args[0] === "join" && perms.check(msg, "rank.join.use")) {
                if (!command.args[1]) {
                    msg.reply(`Por favor, especifique um rank a remover com \`${command.prefix}rank remove \<rank\>\`, para uma lista dos ranks use \`${command.prefix}rank list\``);
                    return true;
                }
                let rankToJoin = command.args[1].toLowerCase();
                if (rankToJoin[0] == "+" || rankToJoin[0] == "-") {
                    rankToJoin = rankToJoin.substring(1);
                }
                let roles = this.config.get("roles", rankToJoin, {server: msg.server.id});
                if (!roles[rankToJoin]) {
                    msg.reply(`Rank inválido, para uma lista dos ranks use \`${command.prefix}rank list\``);
                    return true;
                }
                if (!perms.check(msg, `rank.join.${rankToJoin}`)) {
                    msg.reply(`Por favor, especifique um rank a remover com \`${command.prefix}rank remove \<rank\>\`, para uma lista dos ranks use \`${command.prefix}rank list\``);
                    return true;
                }
                role = msg.server.roles.get("id", roles[rankToJoin]);
                if (role) {
                    this.client.addMemberToRole(msg.author, role, (error)=> {
                        if (error) {
                            let logChannel = this.config.get("msgLog", false, {server: msg.server.id});
                            if (logChannel) {
                                logChannel = msg.server.channels.get("id", logChannel);
                                if (logChannel) {
                                    this.client.sendMessage(logChannel, `Erro ${error} ao promover ${utils.removeBlocks(msg.author.username)} Por favor, redefina o seu rank e veja se o bot tem permissões suficientes.`).catch(console.error)
                                } else {
                                    msg.reply(`Erro ${error} ao promover ${utils.removeBlocks(msg.author.username)} Por favor, redefina o seu rank e veja se o bot tem permissões suficientes.`)
                                }
                            }
                        } else {
                            let logChannel = this.config.get("msgLog", false, {server: msg.server.id});
                            if (logChannel) {
                                logChannel = msg.server.channels.get("id", logChannel);
                                if (logChannel) {
                                    this.client.sendMessage(logChannel, `${utils.removeBlocks(msg.author.username)} adicionou ele mesmo a ${utils.removeBlocks(role.name)}!`)
                                }
                            }
                            msg.reply(":thumbsup::skin-tone-2:");
                        }
                    })
                } else {
                    msg.reply(`O cargo não pode ser encontrado, sendo um administrador use \`${command.prefix}rank add\` para atualizá-lo.`);
                }
                return true;
            }
            if (command.args[0] === "leave" && perms.check(msg, "rank.leave.use")) {
                let roles = this.config.get("roles", command.args[1], {server: msg.server.id});
                if (!command.args[1] || !roles[command.args[1]]) {
                    msg.reply(`Por favor, especifique um rank a remover com \`${command.prefix}rank remove \<rank\>\`, para uma lista dos ranks use \`${command.prefix}rank list\``);
                    return true;
                }
                roles = this.config.get("roles", command.args[1].toLowerCase(), {server: msg.server.id});
                if (!perms.check(msg, `rank.leave.${command.args[1]}`)) {
                    msg.reply(`Você não tem permissão para entrar neste rank. Para uma lista dos ranks use \`${command.prefix}rank list\``);
                    return true;
                }
                role = msg.server.roles.get("id", roles[command.args[1]]);
                if (role) {
                    this.client.removeMemberFromRole(msg.author, role, (error)=> {
                        if (error) {
                            let logChannel = this.config.get("msgLog", false, {server: msg.server.id});
                            if (logChannel) {
                                logChannel = msg.server.channels.get("id", logChannel);
                                if (logChannel) {
                                    this.client.sendMessage(logChannel, `Erro ${error} ao rebaixar ${utils.removeBlocks(msg.author.username)} Por favor, redefina o seu rank e veja se o bot tem permissões suficientes.`).catch(console.error)
                                }
                            }
                        } else {
                            let logChannel = this.config.get("msgLog", false, {server: msg.server.id});
                            if (logChannel) {
                                logChannel = msg.server.channels.get("id", logChannel);
                                if (logChannel) {
                                    this.client.sendMessage(logChannel, `${utils.removeBlocks(msg.author.username)} removeu ele mesmo de ${utils.removeBlocks(role.name)}!`)
                                }
                            }
                            msg.reply(":thumbsup::skin-tone-2:");
                        }
                    })
                } else {
                    msg.reply(`O cargo não pode ser encontrado, sendo um administrador use \`${command.prefix}rank add\` para atualizá-lo.`);
                    return true;
                }
                return true;
            }
        }
        return false;
    }
};
