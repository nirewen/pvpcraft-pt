/**
 * Created by macdja38 on 2016-04-25.
 */
"use strict";

var Utils = require('../lib/utils');
var utils = new Utils();

var color = require('colors');

var Player = require('../lib/player.js');

var key = require('../config/auth.json').youtubeApiKey || null;
if (key == "key") {
    key = null;
}

var text;

module.exports = class music {
    constructor(e) {
        this.client = e.client;
        this.config = e.configDB;
        this.raven = e.raven;
        this.r = e.r;
        this.conn = e.conn;
        /**
         * holds array of servers channels and their bound instances.
         * @type {Array}
         */
        this.boundChannels = [];
    }

    getCommands() {
        return ["init", "play", "skip", "list", "time", "pause", "resume", "volume", "shuffle", "next", "destroy", "logchannel"];
    }

    onDisconnect() {
        for (var i in this.boundChannels) {
            if (this.boundChannels.hasOwnProperty(i))
                this.boundChannels[i].destroy();
        }
    }

    onCommand(msg, command, perms) {
        console.log("Music initiated");
        if (!msg.channel.server) return; //this is a pm... we can't do music stuff here.
        var id = msg.channel.server.id;


        if (command.command === "init" && perms.check(msg, "music.init")) {
            if (this.boundChannels.hasOwnProperty(id)) {
                msg.reply(`Desculpe, j� est� em uso neste servidor. Use ${command.prefix}destroy para finalizar a conex�o atual.`);
                return true;
            }
            if (msg.author.voiceChannel) {
                if (msg.author.voiceChannel.server.id === msg.channel.server.id) {
                    this.boundChannels[id] = new Player({
                        client: this.client,
                        voiceChannel: msg.author.voiceChannel,
                        textChannel: msg.channel,
                        apiKey: key,
                        raven: this.raven,
                        r: this.r,
                        conn: this.conn,
                        config: this.config
                    });
                    msg.reply("Vinculando **" + this.boundChannels[id].voice.name + "** e **" + this.boundChannels[id].text.name + "**");
                    this.boundChannels[id].init(msg, (error)=> {
                        console.log("Bound thing finished maybe");
                        if (error) {
                            console.log(error);
                            msg.reply(error);
                            delete this.boundChannels[id];
                        }
                    });
                }
                else {
                    msg.reply("Voc� precisa estar em um canal de voz pra usar este comando aqui. Se voc� j� est� em um canal, por favor, reentre.")
                }
            }
            else {
                msg.reply("Voc� precisa estar em um canal de voz pra usar este comando. Se voc� j� est� em um canal, por favor, reentre.")
            }
            return true;
        }


        if (command.command === "destroy" && perms.check(msg, "music.destroy")) {
            if (this.boundChannels.hasOwnProperty(id)) {
                this.boundChannels[id].destroy();
                msg.reply("Desconectando do canal de voz e desvinculando o canal de texto");
                delete this.boundChannels[id];
            }
            return true;
        }


        if (command.command === "play" && perms.check(msg, "music.play")) {
            if (this.boundChannels.hasOwnProperty(id) && this.boundChannels[id].hasOwnProperty("connection")) {
                if (this.boundChannels[id].connection.voiceChannel.id !== msg.author.voiceChannel.id) {
                    msg.reply("Voc� precisa estar no canal de texto atual para adicionar uma m�sica a lista. Se voc� j� est� em um canal, por favor, reentre.");
                    return true;
                }
                if (command.args.length > 0) {
                    this.boundChannels[id].enqueue(msg, command.args)
                }
                else {
                    msg.reply("Por favor, especifique um v�deo do YouTube!")
                }
            } else {
                msg.reply("Por favor, conecte um canal de voz primeiro usando " + command.prefix + "init")
            }
            return true;
        }


        if ((command.command === "next" || command.command === "skip") && perms.check(msg, "music.voteskip")) {
            if (this.boundChannels.hasOwnProperty(id) && this.boundChannels[id].hasOwnProperty("connection")) {
                if (this.boundChannels[id].currentVideo) {
                    var index = command.args[0] ? parseInt(command.args[0]) - 1 : -1;
                    console.log(index);
                    var isForced = !!(perms.check(msg, "music.forceskip") && command.flags.indexOf('f') > -1);
                    var video;
                    if (index === -1) {
                        video = this.boundChannels[id].currentVideo;
                    }
                    else if (this.boundChannels[id].queue.hasOwnProperty(index)) {
                        video = this.boundChannels[id].queue[index];
                    }
                    else {
                        msg.reply("N�o foi poss�vel encontrar a m�sica");
                        return true;
                    }
                    if (video.votes.indexOf(msg.author.id) < 0 || isForced) {
                        video.votes.push(msg.author.id);
                        if (video.votes.length > (this.boundChannels[id].connection.voiceChannel.members.length / 3) || isForced) {
                            msg.reply("Removendo " + video.prettyPrint() + " da lista");
                            if (index === -1) {
                                this.boundChannels[id].skipSong();
                            }
                            else {
                                this.boundChannels[id].queue.splice(index, 1);
                            }
                        }
                        else {
                            msg.reply(video.votes.length + " / " + (Math.floor(this.boundChannels[id].connection.voiceChannel.members.length / 3) + 1) + " votos necess�rios para pular" +
                                video.prettyPrint());
                        }
                    }
                    else {
                        msg.reply("Desculpe, voc� s� pode votar para pular uma vez.");
                        return true;
                    }
                }
                else {
                    msg.reply("Nenhuma m�sica na lista, adicione uma usando //play <url do youtube ou uma playlist>");
                    return true;
                }
            } else {
                msg.reply("Por favor, conecte um canal de voz primeiro com " + command.prefix + "init");
                return true;
            }
            return true;
        }


        if (command.command === "pause" && perms.check(msg, "music.pause")) {
            if (this.boundChannels.hasOwnProperty(id) && this.boundChannels[id].hasOwnProperty("connection")) {
                if (this.boundChannels[id].connection.playing) {
                    this.boundChannels[id].pause();
                    msg.reply(`M�sica pausada. Use ${command.prefix}resume para resumir.`)
                } else {
                    msg.reply(`N�o � poss�vel pausar a menos que uma m�sica esteja sendo tocada`)
                }
            } else {
                msg.channel.sendMessage("Desculpe, o bot n�o est� atualmente em um canal de voz. Use " + command.prefix + "init, enquanto estiver em um canal de voz, para conect�-lo.")
            }
            return true;
        }


        if (command.command === "resume" && perms.check(msg, "music.resume")) {
            if (this.boundChannels.hasOwnProperty(id) && this.boundChannels[id].hasOwnProperty("connection")) {
                if (this.boundChannels[id].connection.paused) {
                    this.boundChannels[id].resume(msg);
                    msg.reply("M�sica resumida.")
                } else {
                    msg.reply(`N�o � poss�vel resumir a menos que algo esteja sendo tocado.`)
                }
            } else {
                msg.channel.sendMessage("Desculpe, o bot n�o est� atualmente em um canal de voz. Use " + command.prefix + "init, enquanto estiver em um canal de voz, para conect�-lo.")
            }
            return true;
        }


        if (command.commandnos === "list" && perms.check(msg, "music.list")) {
            if (this.boundChannels.hasOwnProperty(id) && this.boundChannels[id].hasOwnProperty("connection")) {
                if (this.boundChannels[id].currentVideo) {
                    msg.channel.sendMessage("```xl\n" + this.boundChannels[id].prettyList()
                        + "```\n" + this.config.get("website", {musicUrl: "https://pvpcraft.ca/pvpbotmusic/?server="}).musicUrl + msg.server.id, (error)=> {
                        if (error) {
                            console.log(error)
                        }
                    });
                } else {
                    msg.channel.sendMessage("Desculpe, nenhuma m�sica encontrada na lista. Use " + command.prefix + "play <url do youtube ou playlist> para adicionar uma.")
                }
            } else {
                msg.channel.sendMessage("Desculpe, o bot n�o est� atualmente em um canal de voz. Use " + command.prefix + "init, enquanto estiver em um canal de voz, para conect�-lo.")
            }
            return true;
        }


        if (command.commandnos === "time" && perms.check(msg, "music.time")) {
            if (this.boundChannels.hasOwnProperty(id) && this.boundChannels[id].hasOwnProperty("connection")) {
                if (this.boundChannels[id].currentVideo) {
                    msg.channel.sendMessage("Atualmente " + this.boundChannels[id].prettyTime() + " em " + this.boundChannels[id].currentVideo.prettyPrint());
                } else {
                    msg.channel.sendMessage("Desculpe, nenhuma m�sica encontrada na lista. Use " + command.prefix + "play <url do youtube ou playlist> para adicionar uma.")
                }
            } else {
                msg.channel.sendMessage("Desculpe, o bot n�o est� atualmente em um canal de voz. Use " + command.prefix + "init, enquanto estiver em um canal de voz, para conect�-lo.")
            }
            return true;
        }


        if (command.commandnos === "volume") {
            if (this.boundChannels.hasOwnProperty(id) && this.boundChannels[id].hasOwnProperty("connection")) {
                if (command.args[0] && perms.check(msg, "music.volume.set")) {
                    var volume = parseInt(command.args[0]);
                    if (111 > volume && volume > 9) {
                        this.boundChannels[id].setVolume(volume);
                        msg.reply("Volume definido para **" + volume + "**")

                    } else {
                        msg.reply("Desculpe, volume inv�lido, por favor, use um n�mero entre 10 e 110")
                    }
                    return true;
                } else {
                    if (perms.check(msg, "music.volume.list")) {
                        msg.reply("O volume atual � de **" + this.boundChannels[id].getVolume() + "**");
                        return true;
                    }
                    return false;
                }
            } else {
                if (perms.check(msg, "music.volume.list") || perms.check(msg, "music.volume.set")) {
                    msg.channel.sendMessage("Desculpe, o bot n�o est� atualmente em um canal de voz. Use " + command.prefix + "init, enquanto estiver em um canal de voz, para conect�-lo.");
                    return true;
                }
                else {
                    return false;
                }
            }
        }


        if (command.command === "shuffle" && perms.check(msg, "music.shuffle")) {
            if (this.boundChannels.hasOwnProperty(id) && this.boundChannels[id].hasOwnProperty("connection")) {
                if (this.boundChannels[id].queue.length > 1) {
                    msg.channel.sendMessage(this.boundChannels[id].shuffle());
                } else {
                    msg.channel.sendMessage("Desculpe, sem m�sicas suficientes na lista. Adicione mais algumas com" + command.prefix + "play <url do youtube ou playlist>")
                }
            } else {
                msg.channel.sendMessage("Desculpe, o bot n�o est� atualmente em um canal de voz. Use " + command.prefix + "init, enquanto estiver em um canal de voz, para conect�-lo.")
            }
            return true;
        }


        if (command.commandnos === "logchannel" && perms.check(msg, "music.logchannels")) {
            text = "Tocando m�sica em:\n";
            for (var i in this.boundChannels) {
                if (this.boundChannels.hasOwnProperty(i)) {
                    text += `servidor: ${this.boundChannels[i].server.name} no canal de voz ${this.boundChannels[i].text.name}\n`
                }
            }
            if (text != "Tocando m�sica em:\n") {
                msg.channel.sendMessage(text);
            }
            else {
                msg.channel.sendMessage("O bot n�o est� em uso no momento");
            }
            return true;
        }

        return false;
    }
};

